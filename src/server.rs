use std::{
    io::{prelude::*, BufReader},
    net::{IpAddr, Ipv4Addr, TcpListener, TcpStream},
};

mod parser;

static DEFAULT_HOST: IpAddr = IpAddr::V4(Ipv4Addr::UNSPECIFIED);
static DEFAULT_PORT: i32 = 7878;

/// Starts the server and listens for incoming connections.
/// After the last client disconnects, the server will stop after the timeout.
/// If timeout_seconds is None, the server will run indefinitely.
/// If timeout_seconds is Some(n), the server will stop after n seconds of inactivity.
pub(crate) fn start_server(timeout_seconds: Option<u64>) {
    println!("Starting server...");
    let timeout_seconds = timeout_seconds.unwrap_or(0);
    let listen_attempt_result = TcpListener::bind(format!("{DEFAULT_HOST}:{DEFAULT_PORT}"));
    let listener: TcpListener = match listen_attempt_result {
        Ok(listener) => {
            println!("Listening on {}", listener.local_addr().unwrap());
            listener
        }
        Err(e) => {
            println!("Failed to bind to port {DEFAULT_PORT}: {e}");
            return;
        }
    };

    listener
        .set_nonblocking(true)
        .expect("Cannot set non-blocking");

    /*
    There can be only one input device (client) connected at a time.
    This is to prevent multiple clients from sending conflicting commands.
    The server will wait for the current client to disconnect before accepting a new connection.
    */
    let mut last_client_disconnect_time = std::time::Instant::now();
    while timeout_seconds == 0 || last_client_disconnect_time.elapsed().as_secs() < timeout_seconds
    {
        match listener.accept() {
            Ok((stream, addr)) => {
                println!("Connection from {}", addr);
                stream
                    .set_nonblocking(false)
                    .expect("Cannot set blocking on stream");
                handle_connection(stream);
                last_client_disconnect_time = std::time::Instant::now();
            }
            Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                // wait for 100ms, then try again
                std::thread::sleep(std::time::Duration::from_millis(100));
            }
            Err(e) => {
                println!("Failed to accept connection: {}", e);
            }
        }
    }
    if timeout_seconds > 0 {
        println!(
            "Server timed out after {} seconds of inactivity",
            last_client_disconnect_time.elapsed().as_secs()
        );
    }
}

fn handle_connection(mut stream: TcpStream) {
    let buf_reader = BufReader::new(&mut stream);
    for line in buf_reader.lines() {
        let line = line.unwrap();
        println!("[Request] {}", line);
        if line == "DISCONNECT" {
            break;
        }

        //parse and print the request
        let parsed_request = parser::parse_request(&line);
        for command in parsed_request {
            println!("[Command] {}", command);
        }
    }

    stream.write_all(b"Hi\n").unwrap();

    stream.shutdown(std::net::Shutdown::Both).unwrap();

    println!("Connection with {} closed", stream.peer_addr().unwrap());
}

#[cfg(test)]
mod tests {
    use super::*;

    /// Create a client that sends a request (multi-line string) to the server and prints the response.
    #[test]
    fn server_operation_test() {
        // let server_thread = std::thread::spawn(|| start_server(Some(10)));
        // std::thread::sleep(std::time::Duration::from_millis(1000));

        let mut stream =
            TcpStream::connect(format!("{}:{}", Ipv4Addr::LOCALHOST, DEFAULT_PORT)).unwrap();
        let mut command_sequence = Vec::new();
        command_sequence.push("KEY_PRESS:A, KEYCOMBO_PRESS:WIN R, MOUSE_MOVE:100 200, MOUSE_SINGLECLICK, MOUSE_SCROLL:100, MOUSE_RIGHTCLICK\n");
        command_sequence.push("TYPE:Hello World!\n");
        command_sequence.push("DISCONNECT\n");

        for command in command_sequence {
            stream.write_all(command.as_bytes()).unwrap();
            stream.flush().unwrap();
        }

        stream.flush().unwrap();

        // let mut buf_reader = BufReader::new(&mut stream);
        // let mut response = String::new();
        // buf_reader.read_line(&mut response).unwrap();
        // println!("Response: {}", response);

        // assert_eq!(response, "Hi\n");

        stream.write_all(b"DISCONNECT").unwrap();
        stream.flush().unwrap();
        stream.shutdown(std::net::Shutdown::Both).unwrap();

        // server_thread.join().unwrap();
    }
}
