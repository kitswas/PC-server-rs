// mod input_injector;
mod server;

/// Time to stop the server after the last client disconnects.
/// This is to prevent the server from running indefinitely.
/// The server will stop after this many seconds of inactivity.
/// Set to 0 to disable.
#[cfg(debug_assertions)]
static DEFAULT_SERVER_TIMEOUT: u64 = 60; // 1 minute
#[cfg(not(debug_assertions))]
static DEFAULT_SERVER_TIMEOUT: u64 = 5 * 60; // 5 minutes

fn main() {
    server::start_server(Some(DEFAULT_SERVER_TIMEOUT));
    // std::thread::sleep(std::time::Duration::from_secs(10));
    // let mut injector = input_injector::GamepadInjector::new();
    // injector.update(
    //     windows::Gaming::Input::GamepadButtons::DPadUp,
    //     (0.0, 0.0),
    //     (0.0, 0.0),
    // );
    // injector.inject();
    // println!("Injected.");
    // std::thread::sleep(std::time::Duration::from_secs(60));
}
