/// Parses a request into a vector of strings.
/// Here's an example request:
/// KEY_PRESS:A, KEY_RELEASE: A, MOUSE_MOVE:100 200, CLICK, MOUSE_SCROLL:100, RIGHTCLICK
/// TYPE:Hello World!
/// DISCONNECT
pub(super) fn parse_request(request_line: &String) -> Vec<String> {
    //Check for the TYPE command
    //Anything after TYPE: is treated as a string.
    if request_line.starts_with("TYPE:") {
        let request_vec = Vec::from([request_line.to_owned()]);
        return request_vec;
    }

    let request_vec: Vec<String> = request_line
        .split_terminator(",")
        .map(|s| s.trim())
        .map(|s| s.to_string())
        .collect();

    //Each request in the vector is a string of the form "KEY_PRESS:A"
    //We want to split each request into a vector of the form ["KEY_PRESS", "A"]
    //The first element of the vector is the command, and the other elements are the arguments
    // let mut request_vec: Vec<String> = request_vec
    //     .iter()
    //     .map(|s| s.split_terminator(":").map(|s| s.to_string()).collect())
    //     .flatten()
    //     .collect();

    request_vec
}
