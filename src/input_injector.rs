//! Input injection using WinRT.
//! See https://github.com/microsoft/windows-rs
//! and https://learn.microsoft.com/en-us/uwp/api/windows.gaming.input.gamepad

use windows::{
    Gaming::Input::GamepadButtons,
    UI::Input::Preview::Injection::{InjectedInputGamepadInfo, InputInjector},
};

pub(crate) struct GamepadInjector {
    gamepad_state: InjectedInputGamepadInfo,
    injector: InputInjector,
}

impl GamepadInjector {
    pub(crate) fn new() -> Self {
        let injector: InputInjector = InputInjector::TryCreate().unwrap();
        println!("Created input injector.");
        injector.InitializeGamepadInjection().unwrap();
        println!("Initialized gamepad injection.");
        let gamepad_state = InjectedInputGamepadInfo::new().unwrap();
        Self {
            gamepad_state,
            injector,
        }
    }

    pub(crate) fn update(
        &mut self,
        buttons: GamepadButtons,
        left_thumbstick: (f64, f64),
        right_thumbstick: (f64, f64),
    ) {
        let _ = self.gamepad_state.SetButtons(buttons);
        let _ = self.gamepad_state.SetLeftThumbstickX(left_thumbstick.0);
        let _ = self.gamepad_state.SetLeftThumbstickY(left_thumbstick.1);
        let _ = self.gamepad_state.SetRightThumbstickX(right_thumbstick.0);
        let _ = self.gamepad_state.SetRightThumbstickY(right_thumbstick.1);
    }

    pub(crate) fn inject(&mut self) {
        self.injector
            .InjectGamepadInput(&self.gamepad_state)
            .unwrap();
    }
}

impl Drop for GamepadInjector {
    fn drop(&mut self) {
        self.injector.UninitializeGamepadInjection().unwrap();
    }
}

#[cfg(test)]
mod tests {
    use windows::Gaming::Input::Gamepad;

    use super::*;

    /// Use the Gamepad API to detect gamepad input.
    /// This is a sanity check to make sure the API is working.
    /// This test will fail if no gamepad is connected.
    #[test]
    fn test_gamepad_api() {
        let gamepads = Gamepad::Gamepads().unwrap();
        let mut injector = GamepadInjector::new();
        injector.update(
            windows::Gaming::Input::GamepadButtons::DPadUp,
            (0.0, 0.0),
            (0.0, 0.0),
        );
        injector.inject();
        println!("Gamepads: {:?}", gamepads.Size());
        assert!(gamepads.Size().unwrap() > 0);
        let gamepad = gamepads.GetAt(0).unwrap();
        let reading = gamepad.GetCurrentReading().unwrap();
        assert!(reading.Buttons == GamepadButtons::DPadUp);
        std::thread::sleep(std::time::Duration::from_secs(60));
    }
}
